<?php

namespace Drupal\country_flag\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Implements CountryFlagForm.
 */
class CountryFlagForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'country_flag_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the countries with country_flag.countries service.
    // Set $countries to be array in case the Json::decore returns null.
    $countries = (array) \Drupal::service('country_flag.countries')->getCountries();
    $values = $form_state->getValues();
    $form['#attributes'] = ['class' => 'country-flag-wrapper'];
    $form['country'] = [
      '#title' => 'Select a country and see its flag',
      '#type' => 'select',
      '#options' => ['_none' => 'None'] + $countries,
      '#default_value' => isset($values['country']) ? $values['country'] : '_none',
      '#multiple' => FALSE,
      '#ajax' => [
        'event' => 'change',
        'callback' => [$this, 'getCountryFlag'],
      ],
    ];
    // Attach library to the form.
    $form['#attached']['library'] = 'country_flag/init';
    $form['flag'] = [
      '#markup' => '<div id="image-wrapper">You have not selected any country yet</div>',
    ];
    // If there are values and are different than none we render the image if it
    // exists otherwise a non-image found message.
    if (!empty($values) && $values['country'] !== '_none') {
      $flag_url = \Drupal::service('country_flag.countries')->getFlag($values['country']);
      // In case there is no flag, or some empty result.
      if (empty($flag_url)) {
        $html = '<div id="image-wrapper">Flag not available for ' . $values['country'] . '</div>';
      }
      else {
        // Render the image with theme image function.
        $image = [
          '#theme' => 'image',
          '#attributes' => [
            'src' => $flag_url,
            'alt' => $values['country'],
            'width' => 360,
          ],
        ];
        $html = \Drupal::service('renderer')->render($image);
      }
      $form['flag']['#markup'] = '<div id="image-wrapper">' . $html . '</div>';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Return the flag .
   */
  public function getCountryFlag($form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#image-wrapper', drupal_render($form['flag'])));

    return $response;
  }

}
